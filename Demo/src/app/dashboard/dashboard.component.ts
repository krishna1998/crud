import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup, Validators} from '@angular/forms'
import { PersonService } from '../services/person.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public formValue !:FormGroup; 
  public submitted = false;
  public id:any;
  public userObj:'' | undefined;
  public listUsers:any;
  userId: string='';
  userdetails:any;
  newUsers:any;
  dataLoaded:boolean=false;
  addPage: any;
  
 
  
  constructor(private activateRoute: ActivatedRoute, private PersonService:PersonService,private router: Router,private formBuilder: FormBuilder)
   { 
    this.formValue = this.formBuilder.group({
      name:['null',Validators.required],
      Email :['null', Validators.required],
      MobileNumber:['null',Validators.required],
      Description:['null',Validators.required],
     DOB :[Date],
     Gender:['null',Validators.required],
     country:['null',Validators.required],
     Language:['null',Validators.required],
    });
   }

  ngOnInit(): void {
    
   this.getUsers();
  }

  getUsers(){
    this.PersonService.listUsers().subscribe(    
      response =>{
    this.listUsers = response;
     console.log(this.listUsers);
   }
   );
   }
  add()
    {debugger
  // console.log("errormsg")
      // if (this.addPage.invalid) {
      //   this.submitted = true;
      //   //alert('form invalid')
      //   return
      // }
      $('#exampleModal').modal('hide')
      this.PersonService.addUser(this.formValue.value).subscribe(
        response => {
        
         this.getUsers();
        }, err => {
          console.log("error");
        });
    }
 
  addPopup() {
  //  this.submitted = false;
   this.formValue.reset();
    $('#exampleModal').appendTo("body")
  }
 editPopup(){
    //  this.submitted = false;
     this.formValue.reset();
      $('#exampleModal').appendTo("body")
 }
 deletePopup(id:any){
   this.id=id;
console.log(this.id);
   $('exampleModalCenter').appendTo("body").modal("show")
 }
edit(id:any){
  this.id=id;
  if(this.id !== ''){
    this.PersonService.viewuser(this.id)
    .toPromise()
    .then(Response => {
      this.userdetails = Response;
      Object.assign(this.userdetails,Response);
      console.log()
      this.formValue.patchValue(this.userdetails.data[0]);
    })
  }
   
  }

delete(){
  console.log("err");
  this.PersonService.delete(this.id).subscribe(
    response => {
      $('exampleModalCenter').modal('hide');
this.getUsers();

  });

}
}
