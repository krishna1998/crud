import { Injectable } from '@angular/core';
import { HttpClient } from'@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PersonService {
  
 baseUrl: string = 'http://localhost:3003/api/users';
  constructor(private http:HttpClient) { }

  listUsers(){
    return this.http.get(this.baseUrl + '');
  }
  viewuser(id:string){
    return this.http.get(this.baseUrl + '/' + id);
  }
  addUser(userObj:any)
  {
    return this.http.post(this.baseUrl + '/adduser',userObj);
  }
  delete(id:any)
 {
    return this.http.delete(this.baseUrl + '/deluser/'+id);
 }
}

// editUser(userObj:any)
// {
//   return this.http.put(this.baseUrl+ 'editUser/',userObj)
// }






// viewUser(id)
// {
//   return this.http.get(this.baseUrl+ 'users/',id);
// }

// delete(id:any)
// {
//    return this.http.delete(this.baseUrl + '/deluser/',id);
// }








